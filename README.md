# NetSerializer

This depot is an fork of the original NetSerializer git available here : https://github.com/tomba/netserializer

This version support legacy .NET framework 4.0.

---

Building
--------
To build this library, launch build.sh or build.bat (for Windows).
